
 CustomActions = {}

--Better Keys

function AddBetterKeybinds(startIndex)	
	betterKeysIndex = startIndex or 1305
	betterKeysTotal = 0	 
	-- Append Selection to Group

	keyActions['add_group1'] = {action = 'UI_Lua import("/lua/ui/game/selection.lua").AppendSelectionToSet(1)',
					category = 'betterkeymaps', order = betterKeysIndex + betterKeysTotal,}
	CustomActions['add_group1'] = keyActions['add_group1']
	betterKeysTotal = betterKeysTotal + 1

	keyActions['add_group2'] = {action = 'UI_Lua import("/lua/ui/game/selection.lua").AppendSelectionToSet(2)',
					category = 'betterkeymaps', order = betterKeysIndex + betterKeysTotal,}
	betterKeysTotal = betterKeysTotal + 1

	keyActions['add_group3'] = {action = 'UI_Lua import("/lua/ui/game/selection.lua").AppendSelectionToSet(3)',
					category = 'betterkeymaps', order = betterKeysIndex + betterKeysTotal,}
	betterKeysTotal = betterKeysTotal + 1

	keyActions['add_group4'] = {action = 'UI_Lua import("/lua/ui/game/selection.lua").AppendSelectionToSet(4)',
					category = 'betterkeymaps', order = betterKeysIndex + betterKeysTotal,}
	betterKeysTotal = betterKeysTotal + 1

	keyActions['add_group5'] = {action = 'UI_Lua import("/lua/ui/game/selection.lua").AppendSelectionToSet(5)',
					category = 'betterkeymaps', order = betterKeysIndex + betterKeysTotal,}
	betterKeysTotal = betterKeysTotal + 1

	keyActions['add_group6'] = {action = 'UI_Lua import("/lua/ui/game/selection.lua").AppendSelectionToSet(6)',
					category = 'betterkeymaps', order = betterKeysIndex + betterKeysTotal,}
	betterKeysTotal = betterKeysTotal + 1

	keyActions['add_group7'] = {action = 'UI_Lua import("/lua/ui/game/selection.lua").AppendSelectionToSet(7)',
					category = 'betterkeymaps', order = betterKeysIndex + betterKeysTotal,}
	betterKeysTotal = betterKeysTotal + 1

	keyActions['add_group8'] = {action = 'UI_Lua import("/lua/ui/game/selection.lua").AppendSelectionToSet(8)',
					category = 'betterkeymaps', order = betterKeysIndex + betterKeysTotal,}
	betterKeysTotal = betterKeysTotal + 1

	keyActions['add_group9'] = {action = 'UI_Lua import("/lua/ui/game/selection.lua").AppendSelectionToSet(9)',
					category = 'betterkeymaps', order = betterKeysIndex + betterKeysTotal,}
	betterKeysTotal = betterKeysTotal + 1

	keyActions['add_group0'] = {action = 'UI_Lua import("/lua/ui/game/selection.lua").AppendSelectionToSet(0)',
					category = 'betterkeymaps', order = betterKeysIndex + betterKeysTotal,}
	betterKeysTotal = betterKeysTotal + 1

	--

	keyActions['attack_move'] = {action = 'UI_Lua import ("/lua/ui/game/orders.lua").IssueAttackMove()',
					category = 'betterkeymaps', order = betterKeysIndex + betterKeysTotal}
	betterKeysTotal = betterKeysTotal + 1
	
	return betterKeysIndex + betterKeysTotal

end

--Build Mode Keys
function AddBuildModeKeybinds(startindex)
	buildIndex = startindex or (betterKeysIndex + betterKeysTotal)
	buildTotal = 0
	
	keyActions['land_factory'] = {action= 'UI_Lua import("/lua/keymap/hotbuild.lua").buildAction("Land_Factory")',
        category = 'buildmode', order = buildIndex + buildTotal,}
  buildTotal = buildTotal + 1

	keyActions['test1'] = {action = 'UI_Lua import("/lua/ui/game/selection.lua").AppendSelectionToSet(1)',
					category = 'buildmode', order = buildIndex + buildTotal,}
	buildTotal = buildTotal + 1
					
	keyActions['test2'] = {action = 'UI_Lua import("/lua/ui/game/selection.lua").AppendSelectionToSet(1)',
					category = 'buildmode', order = buildIndex + buildTotal,}
	buildTotal = buildTotal + 1
	
	return buildIndex + buildTotal
end


--Build Mode Extra Keys
function AddBuildModeExtraKeybinds(startindex)
	buildExtrasIndex = startindex or (buildIndex + buildTotal)
	buildExtrasTotal = 0
	
	keyActions['test3'] = {action = 'UI_Lua import("/lua/ui/game/selection.lua").AppendSelectionToSet(1)',
					category = 'buildmodeextra', order = buildExtrasIndex + buildExtrasTotal,}
	buildExtrasTotal = buildExtrasTotal + 1
					
	keyActions['test4'] = {action = 'UI_Lua import("/lua/ui/game/selection.lua").AppendSelectionToSet(1)',
					category = 'buildmodeextra', order = buildExtrasIndex + buildExtrasTotal,}
	buildExtrasTotal = buildExtrasTotal + 1
	
	return buildExtrasIndex + buildExtrasTotal
end


--

local index = AddBetterKeybinds(nil)
index = AddBuildModeKeybinds(index)
index = AddBuildModeExtraKeybinds(index)

	