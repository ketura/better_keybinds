
--LOG('Selection successfully hooked.')

function AppendSelectionToSet(name)	
	local function AppendToSet()
		--LOG("Appending selection to group " .. tostring(name))
		local selection = GetSelectedUnits() or {}
		
		AppendSetToSelection(name)
				
		if selectionSets[name] == nil then
			LOG("empty set " .. tostring(name))
		end
		
		--Add Selection to Set appears to be the function that UI_MakeSelectionSet calls, I think, except
		-- it utterly fails to work.  I'm not super hyped about using a console command, but that's what 
		-- the default controls themselves do, so I guess this is as good as it gets.
		--AddSelectionSet(name)
		ConExecute("UI_MakeSelectionSet " .. tostring(name))
	end
	
	--this is a helper function that permits us to select and then deselect units within a single frame, 
	-- completely hidden from the user.
	Hidden(AppendToSet)
end

-- Debug stuff, left in case of future development
-- function Callback(name, unitArray, applied)
-- 	LOG(name .. " set altered")
-- 	LOG(table.getn(unitArray or {}) .. " units in the new set")
-- 	if(applied) then
-- 		LOG("this is a brand new set.")
-- 	else
-- 		LOG("this set was updated.")
-- 	end
-- end

-- RegisterSelectionSetCallback(Callback)

----