

keymap_category_0101 = "Better Keymaps"
keymap_category_0102 = "Build Mode"
keymap_category_0103 = "Build Mode - Extra Keys"

key_desc_2101 = "Adds the current selection to Group 1"
key_desc_2102 = "Adds the current selection to Group 2"
key_desc_2103 = "Adds the current selection to Group 3"
key_desc_2104 = "Adds the current selection to Group 4"
key_desc_2105 = "Adds the current selection to Group 5"
key_desc_2106 = "Adds the current selection to Group 6"
key_desc_2107 = "Adds the current selection to Group 7"
key_desc_2108 = "Adds the current selection to Group 8"
key_desc_2109 = "Adds the current selection to Group 9"
key_desc_2110 = "Adds the current selection to Group 0"

key_desc_2111 = "Issues orders to move to the target point, attacking any enemies along the way."




	
