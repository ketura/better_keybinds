
keyDescriptions['add_group1'] = '<LOC key_desc_2101>Adds the current selection to Group 1'
keyDescriptions['add_group2'] = '<LOC key_desc_2102>Adds the current selection to Group 2'
keyDescriptions['add_group3'] = '<LOC key_desc_2103>Adds the current selection to Group 3'
keyDescriptions['add_group4'] = '<LOC key_desc_2104>Adds the current selection to Group 4'
keyDescriptions['add_group5'] = '<LOC key_desc_2105>Adds the current selection to Group 5'
keyDescriptions['add_group6'] = '<LOC key_desc_2106>Adds the current selection to Group 6'
keyDescriptions['add_group7'] = '<LOC key_desc_2107>Adds the current selection to Group 7'
keyDescriptions['add_group8'] = '<LOC key_desc_2108>Adds the current selection to Group 8'
keyDescriptions['add_group9'] = '<LOC key_desc_2109>Adds the current selection to Group 9'
keyDescriptions['add_group0'] = '<LOC key_desc_2110>Adds the current selection to Group 10'

keyDescriptions['attack_move'] = '<LOC key_desc_2110>Issues orders to move to the target point, attacking any enemies along the way.'