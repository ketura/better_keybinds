
LOG('Selection successfully hooked.')

function AppendSelectionToSet(name)	
	local function AppendToSet()
		LOG("Appending selection to group " .. tostring(name))
		local selection = GetSelectedUnits() or {}
		
		AppendSetToSelection(name)
				
		if selectionSets[name] == nil then
			LOG("empty set " .. tostring(name))
		end
		
		--AddSelectionSet(name)
		ConExecute("UI_MakeSelectionSet " .. tostring(name))
		-- else

		-- 	for _, unit in selection do
		-- 		AddUnitToSelectionSet(tostring(name), unit)
		-- 		unit:AddSelectionSet(name)
		-- 		LOG('adding ', _, ' ', type(unit), ' to ', name)
		-- 	end
		-- end

			
		--AddCurrentSelectionSet(name)

		--ApplySelectionSet(tostring(name))
	end
	
	--this is a helper function that permits us to select and then deselect units within a single frame, completely 
	-- hidden from the user.
	Hidden(AppendToSet)


end


function Callback(name, unitArray, applied)
	LOG(name .. " set altered")
	LOG(table.getn(unitArray or {}) .. " units in the new set")
	if(applied) then
		LOG("this is a brand new set.")
	else
		LOG("this set was updated.")
	end
end


RegisterSelectionSetCallback(Callback)